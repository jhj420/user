GOPATH:=/home/ubuntu/GOPATH
.PHONY: proto
proto:
	sudo protoc --go_out=. --micro_out=. ./proto/user/user.proto

.PHONY: build
build:
	go build -o user *.go

.PHONY: test
test:
	go test -v ./... -cover

.PHONY: docker
docker:
	sudo docker build -t user:latest .

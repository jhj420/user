# Dianshang Service

This is the Dianshang service

Generated with

```
micro new dianshang
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```
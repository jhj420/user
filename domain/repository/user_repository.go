package repository

import (
	"dianshang/domain/model"
	"github.com/jinzhu/gorm"
)

type IUserRepository interface {
	//初始化数据表
	InitTable() error
	//根据用户名查找用户
	FindUserByName(string) (*model.User,error)
	//根据ID查找用户
	FindUserByID(int64) (*model.User,error)
	//创建用户
	CreateUser(*model.User)(int64,error)
	//根据ID删除用户
	DeleteUserByID(int64) error
	//更新用户信息
	UpdateUser(*model.User) error
	//查找所有用户
	FindAll() ([]model.User,error)
}

type UserRepository struct{
	mysqlDB *gorm.DB
}



//创建UserRepository
func NewUserRepository(mysqlDB *gorm.DB) IUserRepository {
	return &UserRepository{mysqlDB: mysqlDB}
}

//创建表
func (u *UserRepository)InitTable() error {
	return u.mysqlDB.CreateTable(&model.User{}).Error
}

//根据用户名查找用户
func (u *UserRepository)FindUserByName(user_name string) (*model.User,error)  {
	user := &model.User{}
	return user,u.mysqlDB.Where("user_name = ?",user_name).Find(user).Error
}
//根据用户ID查找用户
func (u *UserRepository) FindUserByID(id int64) (*model.User, error) {
	user := &model.User{}
	return user,u.mysqlDB.First(user,id).Error

}

func (u *UserRepository) CreateUser(user *model.User) (int64, error) {
	return user.ID,u.mysqlDB.Create(user).Error
}
//根据ID删除用户
func (u *UserRepository) DeleteUserByID(id int64) error {
	return u.mysqlDB.Where("id = ?",id).Delete(&model.User{}).Error
}

func (u *UserRepository) UpdateUser(user *model.User) error {
	return u.mysqlDB.Model(user).Update(&user).Error
}

func (u *UserRepository) FindAll() (userAll []model.User, err error) {
	return userAll,u.mysqlDB.Find(&userAll).Error
}

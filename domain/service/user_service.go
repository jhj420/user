package service

import (
	"dianshang/domain/model"
	"dianshang/domain/repository"
	"errors"
	"golang.org/x/crypto/bcrypt"
)

type IUserService interface {
	AddUser(user *model.User)(int64,error)
	DeleteUser(int64) error
	UpdateUser(user *model.User,isChangePwd bool)(err error)
	FindUserByName(string)(*model.User,error)
	CheckPwd(username string,pwd string)(isOK bool,err error)
}

type UserService struct {
	UserRepository repository.IUserRepository
}

//插入用户
func (u UserService) AddUser(user *model.User) (userID int64, err error) {
	pwdByte,err := GeneratePassword(user.HashPassword)
	if err != nil{
		return user.ID,err
	}
	user.HashPassword = string(pwdByte)
	return u.UserRepository.CreateUser(user)
}
//删除用户
func (u UserService) DeleteUser(userID int64) error {
	return u.UserRepository.DeleteUserByID(userID)
}
//更新用户信息
func (u UserService) UpdateUser(user *model.User, isChangePwd bool) (err error) {
	//判断是否更新密码
	if isChangePwd{
		pwdByte,err := GeneratePassword(user.HashPassword)
		if err != nil{
			return err
		}
		user.HashPassword = string(pwdByte)
	}
	return u.UserRepository.UpdateUser(user)
}
//根据用户名查找用户
func (u UserService) FindUserByName(userName string) (*model.User, error) {
	return u.UserRepository.FindUserByName(userName)
}
//验证密码
func (u UserService) CheckPwd(username string, pwd string) (isOK bool, err error) {
	user,err := u.UserRepository.FindUserByName(username)
	if err != nil{
		return false,err
	}
	return ValidatePassword(pwd,user.HashPassword)
}

func NewUserService(userRepository repository.IUserRepository) IUserService {
	return &UserService{UserRepository: userRepository}
}

//密码加密
func GeneratePassword(userPassword string)([]byte,error)  {
	return bcrypt.GenerateFromPassword([]byte(userPassword),bcrypt.DefaultCost)
}
//验证用户密码
func ValidatePassword(userPassword string,hashed string)(isOK bool,err error)  {
	if err = bcrypt.CompareHashAndPassword([]byte(hashed),[]byte(userPassword));err != nil{
		return false,errors.New("密码错误")
	}
	return true,nil
}

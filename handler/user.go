package handler

import (
	"context"
	"dianshang/domain/model"
	"dianshang/domain/service"
	"dianshang/proto/user"
)

type User struct {
	UserService service.IUserService
}

//用户注册
func (u *User)Register(ctx context.Context, userRegisterRequest *user.UserRegisterRequest,userRegisterResponse *user.UserRegisterResponse ) error{
	userRegister := &model.User{
		UserName:     userRegisterRequest.UserName,
		FirstName:    userRegisterRequest.FirstName,
		HashPassword: userRegisterRequest.Pwd,
	}
	_,err := u.UserService.AddUser(userRegister)
	if err != nil{
		return err
	}
	userRegisterResponse.Message = "注册成功"
	return nil
}
//用户登录
func (u *User) Login(ctx context.Context, userLoginRequest *user.UserLoginRequest, loginResponse *user.UserLoginResponse)  error {
	isOK,err := u.UserService.CheckPwd(userLoginRequest.UserName,userLoginRequest.Pwd)
	if err != nil{
		return err
	}
	loginResponse.IsSuccess=isOK
	return nil
}
//查询用户信息
func (u *User) GetUserInfo(ctx context.Context, userInfoRequest *user.UserInfoRequest, userInfoResponse *user.UserInfoResponse) (error) {
	userInfo,err := u.UserService.FindUserByName(userInfoRequest.UserName)
	if err != nil{
		return err
	}
	userInfoResponse = UserModel2UserInfoResponse(userInfo)
	return nil
}

//类型转换: usermodel-->*user.UserInfoResponse
func UserModel2UserInfoResponse(userModel *model.User) *user.UserInfoResponse {
	userInfoResponse := &user.UserInfoResponse{}
	userInfoResponse.UserName = userModel.UserName
	userInfoResponse.FirstName = userInfoResponse.FirstName
	userInfoResponse.UserId = userModel.ID
	return userInfoResponse
}

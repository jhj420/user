package main

import (
	"dianshang/domain/repository"
	"dianshang/domain/service"
	"dianshang/handler"
	"dianshang/proto/user"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/logger"
)

func main() {
	//创建服务，设置服务参数
	srv := micro.NewService(
		micro.Name("go.micro,service.user"),
		micro.Version("latest"),
	)
	//服务初始化
	srv.Init()

	//创建数据库连接
	db,err := gorm.Open("mysql",
		"root:jiajia123@tcp(127.0.0.1:3306)/micro?charset=utf8&parseTime=True&loc=Local")
	if err != nil{
		fmt.Println(err)
	}
	defer db.Close()
	db.SingularTable(true)

	//数据表初始化，不能重复执行
	//rp := repository.NewUserRepository(db)
	//_ = rp.InitTable()

	userService := service.NewUserService(repository.NewUserRepository(db))
	err = user.RegisterUserHandler(srv.Server(),
		&handler.User{UserService:userService})
	if err != nil{
		logger.Fatal(err)
	}

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
